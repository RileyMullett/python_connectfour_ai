import threading
from time import sleep

from agents.random_agent import random_agent
import game_UI

SPRITE_SCALE = 2
CANT_PLACE = 9999
NOTHING = 0
YELLOW = 1
RED = 2
HUMAN = 0
DRAW_VALUE = 3

STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4


class Game:

    def __init__(self, players, verbose, non_compact, predetermined_start, player_search_depths, stat_creation_run):
        self.player_search_depths = player_search_depths
        self.stat_creation_run = stat_creation_run
        self.players = players
        super().__init__()
        self.board_pieces = [[0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0]]
        self.turn = 1
        self.predetermined_start = predetermined_start
        self.verbose = verbose
        self.non_compact = non_compact
        self.column_count = 7
        self.lastcolumn = 0
        self.lastrow = 0

    def place_piece(self, column):
        x = self.check_free_space_and_create(column=column)
        if x == CANT_PLACE:
            return CANT_PLACE
        else:
            self.lastcolumn = column
            self.lastrow = x
            if self.turn == RED:  # This may look like an inversion but is actually necessary
                return self.check_for_win(colour=YELLOW, column=column, row=x, board_pieces=self.board_pieces)
            if self.turn == YELLOW:
                return self.check_for_win(colour=RED, column=column, row=x, board_pieces=self.board_pieces)

    def check_free_space_and_create(self, column):
        if self.turn is not None:
            for x in range(6):
                if self.board_pieces[column][x] == NOTHING:
                    self.board_pieces[column][x] = self.turn

                    if self.turn == YELLOW:  # This is where the game turns to the next player. (could probably be moved)
                        self.turn = RED
                    else:
                        self.turn = YELLOW
                    return x
        return CANT_PLACE

    def check_for_win(self, colour, column, row, board_pieces):  # There are four win conditions to check for
        countvert = Game.search(self, colour=colour, column=column, row=row, column_modifier=0, row_modifier=1, board_pieces=board_pieces)
        counthoriz = Game.search(self, colour=colour, column=column, row=row, column_modifier=1, row_modifier=0, board_pieces=board_pieces)
        countdiag1 = Game.search(self, colour=colour, column=column, row=row, column_modifier=1, row_modifier=1, board_pieces=board_pieces)
        countdiag2 = Game.search(self, colour=colour, column=column, row=row, column_modifier=1, row_modifier=-1, board_pieces=board_pieces)
        if (countvert > 2) or (counthoriz > 2) or (countdiag1 > 2) or (countdiag2 > 2):
            return colour
        else:
            return 0

    def search(self, colour, column, row, column_modifier, row_modifier, board_pieces) -> int:
        count = 0
        for x in range(1, 4):
            new_column_modifier = column_modifier * x
            new_row_modifier = row_modifier * x
            if (column + new_column_modifier > -1) and (column + new_column_modifier < 7) and (
                    row + new_row_modifier < 6) and (row + new_row_modifier > -1) and (
                    board_pieces[column + new_column_modifier][row + new_row_modifier] == colour):
                count += 1
            else:
                break
        for x in range(1, 4):
            new_column_modifier = column_modifier * x * (-1)
            new_row_modifier = row_modifier * x * (-1)
            if (column + new_column_modifier > -1) and (column + new_column_modifier < 7) and (
                    row + new_row_modifier < 6) and (row + new_row_modifier > -1) and (
                    board_pieces[column + new_column_modifier][row + new_row_modifier] == colour):
                count += 1
            else:
                break
        return count

    def begin(self, view, root, **kwargs) -> str:
        Game_Stat = [[0 for stats in range(5)] for players in range(2)]
        victor = 0
        totalmoves = 0
        statstringarray = ["" for _ in range(2)]

        while victor != 1 and victor != 2 and totalmoves < 6 * 7:
            if not self.predetermined_start == -1:
                choice = self.predetermined_start
                self.predetermined_start = -1
            else:
                choice = self.players[self.turn - 1].take_turn(game=self, player=self.turn, verbose=self.verbose, root=root, non_compact=self.non_compact, search_depth=self.player_search_depths[self.turn - 1], game_stat_cur_player=Game_Stat[self.turn - 1])
            if choice is not None:
                if self.stat_creation_run:
                    board_state_string = ""
                    for y in range(0, 6):
                        for x in range(0, 7):
                            board_state_string = board_state_string + str(self.board_pieces[x][y]) + ","
                    statstringarray[self.turn - 1] = str(statstringarray[self.turn - 1]) + str(board_state_string) + str(choice) + str("!")
                victor = self.place_piece(choice)
                while victor == CANT_PLACE:
                    victor = self.place_piece(self.players[self.turn - 1].take_turn(game=self, player=self.turn, verbose=self.verbose, root=root, non_compact=self.non_compact, search_depth=self.player_search_depths[self.turn - 1], game_stat_cur_player=Game_Stat[self.turn - 1]))  # Try again
                    print('An illegal move was detected, when programming your agent, you need to make sure the space is clear!')
                    sleep(1)
                totalmoves = totalmoves + 1
                if self.turn == RED:
                    view.create_token(column=self.lastcolumn, turn=YELLOW, row=self.lastrow)
                else:
                    view.create_token(column=self.lastcolumn, turn=RED, row=self.lastrow)
                root.update()
            else:
                print('An impossible move was detected, this should NEVER happen')

                """The difference between an illegal move, and an impossible move, is that an illegal move is in a column which is full, whereas an impossible move is not even in one of the seven columns"""
                sleep(1)

        if not self.stat_creation_run:
            if victor == 1:
                Game_Stat[0][STAT_VICTOR] = 1
                Game_Stat[1][STAT_VICTOR] = 0
                return Game_Stat
            elif victor == 2:
                Game_Stat[0][STAT_VICTOR] = 0
                Game_Stat[1][STAT_VICTOR] = 1
                return Game_Stat
            else:
                Game_Stat[0][STAT_VICTOR] = 0
                Game_Stat[1][STAT_VICTOR] = 0
                return Game_Stat
        else:
            db_creation_flipped_start = kwargs.get('db_creation_flipped_start', False)
            if db_creation_flipped_start:   # This part is important, it discards information from games which the neural network loses
                if victor == 2:
                    return statstringarray[1]
            if victor == 1:
                return statstringarray[0]
        return str("")
