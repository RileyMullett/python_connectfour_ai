# TURN = 0
# COLUMN = 1
# ROW = 2
# colours = ['blue', 'yellow', 'red', 'white', 'green']


Tutorial1 = [
    ["Welcome!", None, 'wave.png'],
    ["This program's aim is to assist you in learning about artificial intelligence.", None, 'blank.png'],
    ["This program will cover the basics, including intelligent agents, search,\nheuristics, adversarial agents, optimisation and neural networks.", None, 'blank.png'],
    ["Lets get started!", None, 'blank.png'],
    ["For the context of this program, the intelligent agents described will relate \nto the game of connect four.", [], 'Point3.png'],
    ["An intelligent agent is something which can autonomously perceive an environment,\nand act upon it's perceptions to progress towards a goal state.", None, 'blank.png'],
    ["In connect four, an intelligent agent's goal state is to have four of it's \nmarkers joined in an unbroken line, as shown", [[1, 6, 0], [1, 5, 1], [1, 4, 2], [1, 3, 3], [2, 5, 0], [2, 4, 0], [2, 4, 1], [2, 3, 0], [2, 3, 1], [2, 3, 2], [1, 0, 0], [1, 0, 1]], 'Point2.png'],
    ["Intelligent agents can be categorised by their P.E.A.S. description \nP.E.A.S is shorthand for Performance Measure, Environment, Actuators and Sensors. \nAgents often have more than one attribute in each category.", [[1, 0, 0], [1, 1, 1], [1, 2, 2], [1, 3, 3], [2, 1, 0], [2, 2, 0], [2, 2, 1], [2, 3, 0], [2, 3, 1], [2, 3, 2], [1, 6, 0], [1, 6, 1]], 'peas.png'],
    ["A performance measure is something which the agent is trying to optimise. \nIn the context of connect four, the performance measure would include the number of connected \ntokens. Since more is better it is attempting to maximise this.\nAdditional performance measures could include turn speed and number of blocks.\n Try and think of some more!", [[1, 0, 0], [1, 0, 1], [1, 0, 2], [1, 0, 3], [1, 2, 0], [1, 2, 1], [1, 2, 2], [1, 4, 0], [1, 4, 1], [1, 6, 0]], 'blank.png'],
    ["An environment includes the perceptible and/or dexterous space in which the agent operates and \nall things with which the agent may have an interaction with. \nIn the context of connect four, the environment includes the board space and the opponent's tokens.\n We'll come back to this in a second.", [[4, 0, 0], [4, 0, 5], [4, 6, 0], [4, 6, 5]], 'blank.png'],
    ["The actuators are the ways in which the agent can manipulate the environment. \nIn connect four, there is only one, the column selection \n(Where the agent chooses to place the token).", [[4, 0, 5], [4, 1, 5], [4, 2, 5], [4, 3, 5], [4, 4, 5], [4, 5, 5], [4, 6, 5]], 'Point6.png'],
    ["The sensors are the agent's methods of observing the environment. \nConnect four has only one sensor, the visual perception of the game's board.", [[1, 0, 0], [1, 1, 1], [1, 2, 2], [1, 3, 3], [2, 1, 0], [2, 2, 0], [2, 2, 1], [2, 3, 0], [2, 3, 1], [2, 3, 2], [1, 6, 0], [1, 6, 1]], 'Eye.png'],
    ["Touching back onto environments, they are usually further categorised based\non the following properties: Fully Observable & Partially Observable\nEpisodic & Sequential\nStatic & Dynamic\nDiscrete & Continuous, and\nDeterministic & Stochastic", None, 'blank.png'],
    ["Fully Observable & Partially Observable is exactly what it sounds like.\nCan the agent see everything in its environment or only part of it?\nConnect four is fully observable, whereas battleship is partially observable.", None, 'battleship.png'],
    ["Episodic & Sequential relates to the previous decision made by the agent.\nIf the previous decision impacts the current decision, it is sequential.\nConnect four is sequential.", None, 'sequential.png'],
    ["Static & Dynamic is determined on what can alter the environment.\nIf only the agent in review can alter the environment it is static.\nConnect four is dynamic.", None, 'blank.png'],
    ["Discrete & Continuous relates to the total number of actions which can be taken.\nA discrete environment has a finite number of total actions.\nConnect four is discrete, whereas a taxi drive is continuous", None, 'blank.png'],
    ["Deterministic & Stochastic sorts environments based on whether \nthe future state can be guaranteed by an action. \nIn other words, there is no randomness or variance impacting the future state.\nConnect four is deterministic, because a move is guaranteed to work.", None, 'blank.png'],
    ["Next up, goals and search methods!", None, 'wave.png']
]

Tutorial2 = [
    ["Intelligent agents need some form of logic to find their optimal decision.\nThis involves evaluating one or more 'paths' toward the goal state.\nThis is called a search method.", None, 'search.png'],
    ["Arguably the most simple (and often the least optimal) search methods are\n'depth-first search' and 'width first search'. These methods try every path on every branch\none by one until a goal state is reached. Many other search methods exist,\nthey range in purpose, accuracy, speed and complexity.", None, 'depthfirst.png'],
    ["Search methods can be significantly improved when a heuristic is used.\nA heuristic is some value given to each possible state which reflects\nit's distance to a goal state. This implementation of an agent\n for connect four uses the number of connected counters as a heuristic", None, 'heuristic.png'],
    ["Next up, adversarial agents!", None, 'wave.png']
]
Tutorial3 = [
    ["Adversarial agents are a variant of intelligent agents which are designed to\nmake decisions in an environment against agents with an opposing goal.", None, 'adversarial.png'],
    ["This is the kind of agent which features in this program. \nAn adversarial agent needs to not only consider what will return an optimal score,\nbut also how to prevent any other opposing agents from improving their current state.", None, 'blank.png'],
    ["If you've ever played against a 'bot' in chess, chances are you\nhave had previous experience with an adversarial agent.", None, 'chess.png'],
    ["Click the next tutorial for a deeper dive into a very\ncommon adversarial search algorithm, minimax!", None, 'wave.png']
]

Tutorial4 = [
    ["Minimax is an algorithm designed to make an optimised decision based\non simulating the opponent's decisions.", None, 'wave.png'],
    ["Minimax works as follows:\nA set of every possible future environment state 'N' turns from the present turn is generated.\nThese distant states are evaluated and given a score, usually based on heuristics.", None, 'naive.png'],
    ["For each set, the opponent is simulated, the decision which leads to the lowest score\nout of the the given possibilities is selected to move up to the next tier (they are 'minimising' the score).\n Then it is the agent's turn. They select the maximum score (this is maximising)\nThis continues back and forth all the way back to the immediate state being evaluated", None, 'naive2.png'],
    ["Minimax is strongest when the opponent agent's behavior is directly in opposition, and predictable.\nSimilarly, it is less effective when the opponent is sporadic or poorly understood.\nMinimax in connect four has distinct strengths and weaknesses you can discover using this program.", None, 'app.png'],
    ["In connect four, the branching factor of minimax is 7. That means:\nfor every turn 'x' which minimax looks ahead to,\nan additional 7 to the power of X states need to be evaluated.\nLooking ahead just 5 turns means 16,807 possibilities!", None, 'branching.png'],
    ["Evaluating so many turns can be taxing on computer performance.\nTo remedy this we can use alpha-beta pruning.\nCheck the next tutorial for an explanation.", None, 'wave.png']
]



Tutorial5 = [
    ["Alpha beta pruning is an optimisation method used to improve minimax performance.\nIts a simple concept but can look very complicated.", None, 'alphabeta.png'],
    ["By analysing the logic of minimax it is possible to prove that\ncertain states will never be chosen by minimax.\nBy ignoring these states, or 'pruning' them, you can save a lot of computational time.", None, 'blank.png'],
    ["Alpha is the best already explored option along the path \nto the root for the maximizer. Only a maximizer can\n alter the value of alpha.", None, 'alpha1.png'],
    ["Beta is the best already explored option along the path \nto the root for the minimizer. Only a minimizer can\n alter the value of beta.", None, 'beta1.png'],
    ["While examining the children of a maximizer, \nif the value (new alpha) is greater than beta (from one depth up), \nprune the rest of the children. Otherwise, continue to evaluate.", None, 'alpha2.png'],
    ["While examining the children of a minimizer, \nif the value (new beta) is less than alpha (from one depth up), \nprune the rest of the children. Otherwise, continue to evaluate.", None, 'beta2.png'],
    ["This method will always produce the exact same answer as naive minimax,\nbut can be significantly quicker.\nIt is very rare but possible for it to take just as\nlong as naive minimax, if there are no branches fit to prune.", None, 'wave.png'],
]

Tutorial6 = [
    ["Neural Networks are outside the scope of your course...\nbut if you're interested, a neural network can be deployed as an intelligent agent.\nNeural Networks are bio-mimicry of how neurons in a brain behave.", None, 'ai.png'],
    ["The neural network in this program is trained by machine learning.\nMachine learning involves building up the classifier by inputting a\nvolume of data and their classifications, until the\nnetwork builds itself to find the pattern.", None, 'blank.png'],
    ["Once it has trained it can then use the classifier it created to evaluate new data.\nThe neural network in this game trains off data generated from\nthe minimax alphabeta agent's decisions.\nThis means that any faults which minimax has will likely\nexist in the neural network player.", None, 'blank.png'],
    ["Neural networks can not be debugged, because their contents are not human-made or human-readable.\nTo build a good neural network requires good data.\nhaving too little data can create a data overfit, making\nthe network less adaptive. Having too much data can make training slow, and it can be\ndifficult to find enough data to input to begin with.", None, 'blank.png'],
    ["You can watch the neural network gather training data and train using the buttons\nlabelled 'gather training data' and 'train neural network' in the menu.", None, 'wave.png']
]

Tutorial7 = [
    ["Want to make your own adversarial agent? Great! All you need is a little Python background knowledge.\nCopy the 'user_custom.py' file from inside the 'agents' folder.", None, 'wave.png'],
    ["Open this file. It contains a class which every other agent follows the structure of.\nLook at the other agents for examples, and then write your own!\nYour new *agent*.py will automatically be added. ", None, 'blank.png'],
    ["To test your agent, select it from the dropdown menu, and then click 'run test'\nYour agent will be pitted against another, and the statistics\n will be printed to the console at the end.", None, 'blank.png'],
    ["As an aside, if you see any bugs in my code, you can fix those while you're at it ;)", None, 'wave.png']
]