import tkinter
from PIL import Image

SPRITE_SCALE = 2
CANT_PLACE = 9999
NOTHING = 0
YELLOW = 1
RED = 2
HUMAN = 0
colours = ['blue', 'yellow', 'red', 'white', 'green']
BOARD_CELL_SIZE = 60
BOARD_CHIP_SIZE_HIGH = BOARD_CELL_SIZE - 6
BOARD_CHIP_SIZE_LOW = 6

DESCRIPTION = 0
TOKENS = 1
IMAGE = 2

TURN = 0
COLUMN = 1
ROW = 2


class tutorial_ui(tkinter.Toplevel):

    def __init__(self, root):
        super().__init__(root)
        self.title = 'Active Game Display'
        self.column_count = 7
        self.row_count = 7
        self.board = tkinter.Frame(self, background=colours[3], width=(self.column_count + 2) * BOARD_CELL_SIZE, height=self.row_count * BOARD_CELL_SIZE)
        self.grid_space = [[tkinter.Canvas for _ in range(7)] for _ in range(9)]

    def create_token(self, column, turn, row):
        self.grid_space[column+1][row].create_oval(BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_HIGH, BOARD_CHIP_SIZE_HIGH, fill=colours[turn])

    def setup(self, tutorial_array, index=0):
        if index >= len(tutorial_array):
            self.destroy()
        else:
            self.board.destroy()
            self.board = tkinter.Frame(self, background=colours[3], width=(self.column_count + 2) * BOARD_CELL_SIZE, height=self.row_count * BOARD_CELL_SIZE)
            self.board.grid(column=0)
            if tutorial_array[index][IMAGE] is not None:
                load = Image.open('resources/'+tutorial_array[index][IMAGE])
                render = PhotoImage(load)
                img = tkinter.Label(self.board, image=render, background=colours[3])
                img.image = render
                img.grid(column=self.column_count+3, rowspan=self.row_count+1, sticky='nsw')
            board_base = tkinter.Canvas(self.board, bg='white', width=(self.column_count + 2) * BOARD_CELL_SIZE, height=BOARD_CELL_SIZE * 2)
            for y in range(2, self.row_count - 1):
                self.board.rowconfigure(y, minsize=BOARD_CELL_SIZE, weight=0)
            for y in range(self.row_count - 1):
                for x in range(self.column_count):
                    self.grid_space[x + 1][y] = tkinter.Canvas(self.board, background=colours[0], width=BOARD_CELL_SIZE, height=BOARD_CELL_SIZE, highlightthickness=0, relief='ridge')
            if tutorial_array[index][TOKENS] is not None:
                for x in range(self.column_count + 2):
                    self.board.columnconfigure(x, minsize=BOARD_CELL_SIZE, weight=0)
                board_base.config(highlightthickness=0)
                board_base.create_polygon(0, (BOARD_CELL_SIZE / 2), BOARD_CELL_SIZE * 2, (BOARD_CELL_SIZE / 2) * 3, (self.column_count + 2) * BOARD_CELL_SIZE, (BOARD_CELL_SIZE / 2) * 3, self.column_count * BOARD_CELL_SIZE, (BOARD_CELL_SIZE / 2), fill=colours[0])
                board_base.grid(column=0, row=self.row_count, columnspan=9, rowspan=2, sticky="nw")
                for x in range(self.column_count):
                    for y in range(self.row_count - 1):
                        self.grid_space[x + 1][y].create_oval(BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_HIGH, BOARD_CHIP_SIZE_HIGH, fill=colours[3])
                        self.grid_space[x + 1][y].grid(row=(self.row_count - y), column=x + 1, sticky="nw")
                for token in tutorial_array[index][TOKENS]:
                    self.create_token(token[COLUMN], token[TURN], token[ROW])

            next_button = tkinter.Button(
                master=self.board,
                bg=colours[3],
                command=lambda: self.setup(tutorial_array, index + 1)
            )
            if index+1 >= len(tutorial_array):
                next_button['text'] = 'Finish'
            else:
                next_button['text'] = 'Next'
            information = tkinter.Label(master=self.board, width=80, text=tutorial_array[index][DESCRIPTION], bg='white')
            information.grid(column=0, row=self.row_count + 2, columnspan=11,  sticky="ews")
            next_button.grid(column=0, row=self.row_count + 3, columnspan=11, sticky="ews")

