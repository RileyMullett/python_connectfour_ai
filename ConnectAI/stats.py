from datetime import datetime, time
from math import floor

STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4
LEFT_PLAYER = 0
RIGHT_PLAYER = 1


def rounds_descriptions(cur_round) -> str:
    column = floor((cur_round % 14) / 2)
    if (cur_round + 2) % 2 > 0:
        return "Game " + str(cur_round) + " (Right player starts, first move column " + str(column) + ")\n"
    else:
        return "Game " + str(cur_round) + " (Left player starts, first move column " + str(column) + ")\n"


def collate_data_for_player(player, player_string: str, test_stats, repeats):
    TOTAL_ROUNDS = 14 * repeats
    TOTAL_ROUNDS_HALF = 7 * repeats

    log_str =("\n\n---------------------------------------------------------------------------------\n")
    other_player = 1
    if player == LEFT_PLAYER:
        log_str +=("Data for " + player_string + " (left player)\n")
    else:
        log_str +=("Data for " + player_string + " (right player)\n")
        other_player = 0
    log_str +=("\n\n")
    log_str +=("--Win rate--\n")
    win_rate = 0
    for Round in range(0, TOTAL_ROUNDS):
        win_rate += test_stats[Round][player][STAT_VICTOR]
    win_rate = win_rate * 100 / TOTAL_ROUNDS
    log_str +=("Win rate overall: " + str(win_rate)[:6] + "%\n")
    win_rate2 = 0
    for Round in range(0, TOTAL_ROUNDS):
        if Round % 2 == 1:
            if player == RIGHT_PLAYER:
                win_rate2 += test_stats[Round][player][STAT_VICTOR]
        else:
            if player == LEFT_PLAYER:
                win_rate2 += test_stats[Round][player][STAT_VICTOR]
    win_rate2 = win_rate2 * 100 / TOTAL_ROUNDS_HALF
    log_str +=("Win rate as player 1: " + str(win_rate2)[:6] + "%\n")
    win_rate3 = 0
    for Round in range(0, TOTAL_ROUNDS):
        if Round % 2 == 1:
            if player == LEFT_PLAYER:
                win_rate3 += test_stats[Round][player][STAT_VICTOR]
        else:
            if player == RIGHT_PLAYER:
                win_rate3 += test_stats[Round][player][STAT_VICTOR]
    win_rate3 = win_rate3 * 100 / TOTAL_ROUNDS_HALF
    log_str +=("Win rate as player 2: " + str(win_rate3)[:6] + "%\n")
    draw_rate = 0
    for Round in range(0, TOTAL_ROUNDS):
        if test_stats[Round][LEFT_PLAYER][STAT_VICTOR] == 0 and test_stats[Round][RIGHT_PLAYER][STAT_VICTOR] == 0:
            draw_rate += 1
    draw_rate = draw_rate * 100 / TOTAL_ROUNDS
    log_str +=("Draw rate overall: " + str(draw_rate)[:6] + "%\n")
    draw_rate2 = 0
    for Round in range(0, TOTAL_ROUNDS):
        if Round % 2 == 1:
            if player == RIGHT_PLAYER:
                if test_stats[Round][LEFT_PLAYER][STAT_VICTOR] == 0 and test_stats[Round][RIGHT_PLAYER][STAT_VICTOR] == 0:
                    draw_rate2 += 1
        else:
            if player == LEFT_PLAYER:
                if test_stats[Round][LEFT_PLAYER][STAT_VICTOR] == 0 and test_stats[Round][RIGHT_PLAYER][STAT_VICTOR] == 0:
                    draw_rate2 += 1
    draw_rate2 = draw_rate2 * 100 / TOTAL_ROUNDS_HALF
    log_str +=("Draw rate as player 1: " + str(draw_rate2)[:6] + "%\n")
    draw_rate3 = 0
    for Round in range(0, TOTAL_ROUNDS):
        if Round % 2 == 1:
            if player == LEFT_PLAYER:
                if test_stats[Round][LEFT_PLAYER][STAT_VICTOR] == 0 and test_stats[Round][RIGHT_PLAYER][STAT_VICTOR] == 0:
                    draw_rate3 += 1
        else:
            if player == RIGHT_PLAYER:
                if test_stats[Round][LEFT_PLAYER][STAT_VICTOR] == 0 and test_stats[Round][RIGHT_PLAYER][STAT_VICTOR] == 0:
                    draw_rate3 += 1
    draw_rate3 = draw_rate3 * 100 / TOTAL_ROUNDS_HALF
    log_str +=("Draw rate as player 2: " + str(draw_rate3)[:6] + "%\n")
    log_str +=("Loss rate overall: " + str(100 - (win_rate + draw_rate))[:6] + "%\n")
    log_str +=("Loss rate as player 1: " + str(100 - (win_rate2 + draw_rate2))[:6] + "%\n")
    log_str +=("Loss rate as player 2: " + str(100 - (win_rate3 + draw_rate3))[:6] + "%\n")
    log_str +=("\n\n")
    log_str +=("--Time--\n")
    turn_count = 0
    for Round in range(0, TOTAL_ROUNDS):
        turn_count += test_stats[Round][player][STAT_TURN_TIME]
    turn_count = turn_count / TOTAL_ROUNDS
    log_str +=("Per game turn evaluation time average: " + str(turn_count)[:6] + " Seconds\n")
    turn_count2 = 0
    for Round in range(0, TOTAL_ROUNDS):
        turn_count2 += test_stats[Round][other_player][STAT_TURN_TIME]
    turn_count2 = turn_count2 / TOTAL_ROUNDS
    if turn_count2 != 0:
        turn_count2 = turn_count / turn_count2
        log_str +=("Turn evaluation time ratio (Lower is faster): " + str(turn_count2)[:6] + ":1\n")
    turn_count3 = 99999
    turn_count4 = -1
    turn_count5 = -99999
    turn_count6 = -1
    for Round in range(0, TOTAL_ROUNDS):
        if test_stats[Round][player][STAT_VICTOR] == 1:
            if test_stats[Round][player][STAT_TURN_TIME] < turn_count3:
                turn_count3 = test_stats[Round][player][STAT_TURN_TIME]
                turn_count4 = Round
            if test_stats[Round][player][STAT_TURN_TIME] > turn_count5:
                turn_count5 = test_stats[Round][player][STAT_TURN_TIME]
                turn_count6 = Round
    if not turn_count3 == 99999:
        log_str +=("Fastest won game: " + rounds_descriptions(turn_count4) + " with a time of: " + str(turn_count3)[:6] + " Seconds\n")
    if not turn_count5 == -99999:
        log_str +=("Slowest won game: " + rounds_descriptions(turn_count6) + " with a time of: " + str(turn_count5)[:6] + " Seconds\n")
    log_str +=("\n")
    log_str +=("--Moves--\n")

    move_time = 0
    for Round in range(0, TOTAL_ROUNDS):
        move_time += test_stats[Round][player][STAT_TURNS]
    move_time = move_time / TOTAL_ROUNDS
    log_str +=("Average moves per game: " + str(move_time)[:6]+"\n")
    move_time2 = 99999
    move_time3 = -1
    move_time4 = -99999
    move_time5 = -1
    for Round in range(0, TOTAL_ROUNDS):
        if test_stats[Round][player][STAT_VICTOR] == 1:
            if test_stats[Round][player][STAT_TURNS] < move_time2:
                move_time2 = test_stats[Round][player][STAT_TURNS]
                move_time3 = Round
            if test_stats[Round][player][STAT_TURNS] > move_time4:
                move_time4 = test_stats[Round][player][STAT_TURNS]
                move_time5 = Round
    if not move_time4 == 99999:
        log_str +=("Most moves to win: " + rounds_descriptions(move_time5) + " with " + str(move_time4) + " turns.\n")
    if not move_time2 == 99999:
        log_str +=("Fewest moves to win: " + rounds_descriptions(move_time3) + " with " + str(move_time2) + " turns.\n")
    log_str +=("\n")
    prune_total = 0
    for Round in range(0, TOTAL_ROUNDS):
        prune_total += test_stats[Round][player][STAT_BRANCH_PRUNES]
    prune_total2 = prune_total / TOTAL_ROUNDS
    prune_total3 = prune_total2 / move_time
    if prune_total > 0:
        log_str +=("--Pruning--\n")
        log_str +=("Total branch prune occurrences: " + str(prune_total)+"\n")
        log_str +=("Average branch prune occurrences per game: " + str(prune_total2)[:6]+"\n")
        log_str +=("Average branch prune occurrences per turn: " + str(prune_total3)[:6]+"\n")
        log_str +=("\n")
    # log_str +=("--Defence--\n")     Commented out because it didn't actually mean anything in the first place, in hindsight.
    # defence_total = 0
    # defence_total3 = -1
    # defence_total4 = -99999
    # defence_total5 = -1
    # defence_total6 = 99999
    # for Round in range(0, TOTAL_ROUNDS):
    #     defence_total += test_stats[Round][player][STAT_FORCED_DEFENCES]
    #     if test_stats[Round][player][STAT_FORCED_DEFENCES] > defence_total4:
    #         defence_total4 = test_stats[Round][player][STAT_FORCED_DEFENCES]
    #         defence_total3 = Round
    #     if test_stats[Round][player][STAT_FORCED_DEFENCES] < defence_total6:
    #         defence_total6 = test_stats[Round][player][STAT_FORCED_DEFENCES]
    #         defence_total5 = Round
    # defence_total2 = defence_total / TOTAL_ROUNDS
    # log_str +=("Total times forced to defend: " + str(defence_total))
    # log_str +=("Average times forced to defend per game: " + str(defence_total2)[:6])
    # log_str +=("Most forced defences in a game: " + rounds_descriptions(defence_total3) + " with " + str(defence_total4)[:6] + " forced defences.\n")
    # log_str +=("Least forced defences in a game: " + rounds_descriptions(defence_total5) + " with " + str(defence_total6)[:6] + " forced defences.\n")

    print(log_str)
    with open(player_string+"_Game_Stats_"+datetime.now().strftime("%d-%m-%y_%H-%M-%S")+".txt", "w") as text_file:
        print(log_str, file=text_file)
    print("Game stats saved to local directory.")
    text_file.close()