from tkinter import *
from tkinter import ttk
import numbers
from game import Game

SPRITE_SCALE = 2
NOTHING = 0
YELLOW = 1
RED = 2
HUMAN = 0
colours = ['blue', 'yellow', 'red']
BEST_SCORE = 0
BEST_COLUMN = 1
CHILD_TREES = 2
NUM_NONE = -999999

class Branches_UI(Toplevel):

    def __init__(self, game: Game, root: ttk, compute_time, max_depth):
        super().__init__(master=root)
        self.title = 'Decision Tree Explorer'
        self.Game = game
        self.compute_time = compute_time
        self.column_count = 7
        self.max_depth = max_depth
        self.resizable(True, True)
        self.Continue = False
        self.mainFrame = Frame(self)
        self.buttonFrame = Frame(self)
        self.continue_game = False
        self.tree_grid = None
        self.full_search_tree_canvas = None

    def create_token_compact(self, column, players_turn: bool, value, row, chosen_tree, make_button: bool, decision_tree, branch_above, non_compact: bool):
        if value is not None:
            token = Canvas(self.tree_grid, width=30, height=90, highlightthickness=0, relief='ridge')
            if players_turn:
                token.create_polygon(1, 30, 15, 1, 30, 30, fill='navy')
                token.create_text(15, 20, text=value, justify=CENTER, fill='white')
            else:
                token.create_polygon(1, 1, 15, 30, 30, 1, fill='SpringGreen4')
                token.create_text(15, 12, text=value, justify=CENTER, fill='white')
            if make_button:
                if chosen_tree[CHILD_TREES] != NUM_NONE:
                    next_depth = Depth_Selector(branches_ui=self, chosen_tree=chosen_tree, decision_tree=decision_tree, players_turn=players_turn, branch_above=branch_above, non_compact=non_compact)
                    next_depth2 = Button(
                        master=self.tree_grid,
                        text='▼',
                        command=next_depth.on_click
                    )
                    next_depth2.grid(row=row, column=column)
            token.grid(row=row, column=column)

    def create_prune(self, column, row):
        token = Canvas(self.tree_grid, width=30, height=60, highlightthickness=0, relief='ridge')
        token.create_polygon(2, 29, 0, 27, 12, 15, 12, 14, 0, 2, 2, 0, 14, 12, 15, 12, 27, 0, 29, 2, 17, 14, 17, 15, 29, 27, 27, 29, 15, 17, 14, 17, 2, 29, fill='red')
        token.grid(row=row, column=column)

    def setup(self, decision_tree, current_branch, branch_above, players_turn: bool, non_compact: bool, is_neural_net: bool):
        if non_compact:
            self.max_depth = min(4, self.max_depth)  # if this is set to max_depth passed by init, the tree can be as big as you like. Expect serious consequences from this.
        self.mainFrame.destroy()
        self.buttonFrame.destroy()

        self.mainFrame = Frame(self)
        self.mainFrame.pack(fill=BOTH, expand=True)
        self.buttonFrame = Frame(self)
        home = Depth_Selector(branches_ui=self, chosen_tree=decision_tree, decision_tree=decision_tree, players_turn=True, branch_above=None, non_compact=non_compact)  # First turn is a max
        Time_Taken = Label(
            self.buttonFrame,
            text='Computation Took ' + str(self.compute_time) + ' seconds',
        )
        Time_Taken.pack()
        home3 = Button(
            master=self.buttonFrame,
            text='Continue Game',
            command=self.continue_click
        )
        home3.pack()
        if not non_compact:
            home2 = Button(
                master=self.buttonFrame,
                text='Top Of Tree',
                command=home.on_click
            )
            home2.pack()
        self.buttonFrame.pack()
        mainCanvas = Canvas(self.mainFrame)
        self.tree_grid = Frame(mainCanvas)
        if non_compact:
            scrollbar = Scrollbar(self.mainFrame, orient='vertical', command=mainCanvas.yview)
            scrollbar2 = Scrollbar(self.mainFrame, orient='horizontal', command=mainCanvas.xview)
            mainCanvas.configure(yscrollcommand=scrollbar.set, xscrollcommand=scrollbar2.set)
            scrollbar.pack(side='right', fill='y')
            scrollbar2.pack(side='bottom', fill='x')
        mainCanvas.pack(side='left', fill=BOTH, expand=True)
        mainCanvas.create_window(0, 0, window=self.tree_grid, anchor='nw')
        self.tree_grid.bind('<Configure>', lambda event: mainCanvas.configure(scrollregion=mainCanvas.bbox("all")))
        if non_compact:
            current_branch = decision_tree
            index = [0] * (self.max_depth + 2)
            self.full_search_tree_canvas = Canvas(self.tree_grid, width=30 * 7 ** self.max_depth, height=(self.max_depth * 70)+50, highlightthickness=0, relief='ridge')
            self.create_full_search_tree(players_turn, current_branch, 0, index, is_neural_net)
            self.full_search_tree_canvas.grid(sticky="nw")
        else:
            for COLUMN in range(0, 7):
                if current_branch != NUM_NONE:
                    if current_branch[CHILD_TREES][COLUMN] != NUM_NONE:
                        self.create_token_compact(column=COLUMN, players_turn=not players_turn, value=current_branch[BEST_SCORE], row=2, chosen_tree=current_branch[CHILD_TREES][COLUMN], branch_above=current_branch, make_button=True, decision_tree=decision_tree, non_compact=non_compact)
                    else:
                        self.create_prune(column=COLUMN, row=2)
                    if branch_above is not None and branch_above != NUM_NONE and branch_above[BEST_COLUMN] != NUM_NONE:
                        self.create_token_compact(column=branch_above[BEST_COLUMN], players_turn=not players_turn, value=branch_above[BEST_SCORE], row=0, chosen_tree=current_branch, branch_above=current_branch, make_button=False, decision_tree=decision_tree, non_compact=non_compact)
                    if current_branch[BEST_COLUMN] != NUM_NONE:
                        self.create_token_compact(column=current_branch[BEST_COLUMN], players_turn=players_turn, value=current_branch[BEST_SCORE], row=1, chosen_tree=current_branch, branch_above=current_branch, make_button=False, decision_tree=decision_tree, non_compact=non_compact)

    def continue_click(self):
        self.continue_game = True
        self.destroy()

    def create_full_search_tree(self, players_turn: bool, current_branch, depth, index, is_neural_net: bool, last_x=None, last_y=None, valid_column=None):
        pyramid_offset = ((7 ** self.max_depth) / (7 ** depth)) / 2
        next_value_offset = index[depth] * ((7 ** self.max_depth) / (7 ** depth))
        col_val = int(pyramid_offset + next_value_offset)
        x = col_val * 30
        y = depth * 50

        if not isinstance(current_branch, numbers.Integral):
            self.create_full_search_token(y=y, players_turn=players_turn, value=current_branch[BEST_SCORE], x=x, token=self.full_search_tree_canvas, is_neural_net=is_neural_net, is_prune=False)
            if last_x is not None:
                if valid_column:
                    self.full_search_tree_canvas.create_line(last_x + 16, last_y + 30, x + 16, y, fill='green', width=2)
                else:
                    self.full_search_tree_canvas.create_line(last_x + 16, last_y + 30, x + 16, y, fill='black', width=2)
            if depth < self.max_depth and not isinstance(current_branch[CHILD_TREES], numbers.Integral):
                for COLUMN in range(0, 7):
                    valid_column = (COLUMN == current_branch[BEST_COLUMN])
                    index = self.create_full_search_tree(not players_turn, current_branch[CHILD_TREES][COLUMN], depth + 1, index, is_neural_net=is_neural_net, last_x=x, last_y=y, valid_column=valid_column)
            else:
                head_depth = depth-1
                for x in range(depth, self.max_depth):
                    index[x+1] += 7**(x-head_depth)


        else:
            self.create_full_search_token(y=y, players_turn=players_turn, value=0, x=x, token=self.full_search_tree_canvas, is_neural_net=is_neural_net, is_prune=True)
            if last_x is not None:
                self.full_search_tree_canvas.create_line(last_x + 16, last_y + 30, x + 16, y, fill='red', width=2)
            last_x = x
            last_y = y
            counter = 1
            for DEPTH_VAR in range(depth+1, self.max_depth + 1):  # Not pretty, but Tkinter will not draw an empty grid.
                for count in range(0, 7 ** counter):
                    pyramid_offset = ((7 ** self.max_depth) / (7 ** DEPTH_VAR)) / 2
                    next_value_offset = index[DEPTH_VAR] * ((7 ** self.max_depth) / (7 ** DEPTH_VAR))
                    col_val = int(pyramid_offset + next_value_offset)
                    x = col_val * 30
                    y = (DEPTH_VAR * 50)
                    self.create_full_search_token(y=y, players_turn=players_turn, value=0, x=x, token=self.full_search_tree_canvas, is_neural_net=is_neural_net, is_prune=True)
                    self.full_search_tree_canvas.create_line(last_x + 16, last_y + 30, x + 16, y, fill='red', width=2)
                    index[DEPTH_VAR] += 1
                counter += 1
                last_x = x
                last_y = y
        index[depth] += 1
        return index

    def create_full_search_token(self, y, players_turn: bool, value, x, token: Canvas, is_neural_net: bool, is_prune: bool):
        if is_neural_net:
            token.create_polygon(x + 1, y + 30, x + 30, y + 30, x + 30, y + 1, x + 1, y + 1, fill='navy')
            token.create_text(x + 15, y + 20, text=value, justify=CENTER, fill='white')
        elif is_prune:
            token.create_polygon(x + 2, y + 29, x + 0, y + 27, x + 12, y + 15, x + 12, y + 14, x + 0, y + 2, x + 2, y + 0, x + 14, y + 12, x + 15, y + 12, x + 27, y + 0, x + 29, y + 2, x + 17, y + 14, x + 17, y + 15, x + 29, y + 27, x + 27, y + 29, x + 15, y + 17, x + 14, y + 17, x + 2, y + 29, fill='red')
        elif players_turn:
            token.create_polygon(x + 1, y + 30, x + 15, y + 1, x + 30, y + 30, fill='navy')
            token.create_text(x + 15, y + 20, text=value, justify=CENTER, fill='white')
        else:
            token.create_polygon(x + 1, y + 1, x + 15, y + 30, x + 30, y + 1, fill='SpringGreen4')
            token.create_text(x + 15, y + 12, text=value, justify=CENTER, fill='white')


class Depth_Selector:
    def __init__(self, branches_ui, chosen_tree, decision_tree, players_turn: bool, branch_above, non_compact: bool):
        super().__init__()
        self.Branches_UI = branches_ui
        self.chosen_tree = chosen_tree
        self.decision_tree = decision_tree
        self.players_turn = players_turn
        self.branch_above = branch_above
        self.non_compact = non_compact

    def on_click(self):
        try:
            self.Branches_UI.setup(self.decision_tree, current_branch=self.chosen_tree, players_turn=self.players_turn, branch_above=self.branch_above, non_compact=self.non_compact, is_neural_net=False)
        except IOError:
            pass  # Expected to occur
