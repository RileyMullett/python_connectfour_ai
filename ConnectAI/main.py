import os
import threading
import tkinter
from tkinter import *
from tkinter import ttk
import game_UI
from time import sleep
from game import Game
from stats import collate_data_for_player
from random import randint
from copy import deepcopy
import tutorials
import tutorial_ui
from agents.minimax_alphabeta_unpredictable import minimax_alphabeta_unpredictable
from agents.human import human
from agents.neural_net import neural_net

SelectionPlayers = [human]
SelectionStrings = ['human']
for entry in os.scandir('agents'):
    if entry.is_file():
        if str(entry.name[:-3]) != "genericPlayer" and str(entry.name[:-3]) != "human":  # This was a roundabout way to ensure that the Human agent was always the default on launch.
            string_cmd = f'from agents.{entry.name}'[:-3] + f' import {entry.name}'[:-3]
            exec(string_cmd)
            string_cmd = f'SelectionPlayers.append({entry.name}'[:-3] + ")"
            exec(string_cmd)
            SelectionStrings.append(entry.name[:-3])

# Constants
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Connect 4 AI"
LEFT_PLAYER = 0
RIGHT_PLAYER = 1
STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4


class ConnectFourAI(tkinter.Tk):
    lbox2 = ttk.Combobox
    lbox1 = ttk.Combobox
    test_repeats = ttk.Entry
    Test_Stats = []
    window = Toplevel
    """
    Creates instance of connect four app, starting at main menu.
    """
    frame = Frame

    def __init__(self):  # Instantiating window
        super().__init__()
        self.title(SCREEN_TITLE)
        self.verbose = IntVar()
        self.rand_first_start = IntVar()
        self.depth_player_1 = IntVar()
        self.depth_player_2 = IntVar()
        self.verbose_compact = IntVar()
        self.rand_first_player = IntVar()
        self.epilepsy_control = IntVar()

    def set_up(self):  # Sets up game
        self.configure(bg='white')
        frame = Frame(self, width=SCREEN_WIDTH, height=SCREEN_HEIGHT, bg='white')
        for x in range(7):
            frame.rowconfigure(x, minsize=35, weight=0)
        frame.rowconfigure(8, minsize=50, weight=0)
        frame.rowconfigure(9, minsize=35, weight=0)
        title = Label(
            frame,
            text='Connect 4 AI',
            font=("Arial", 18),
            bg='white'
        )
        title.grid(sticky='se',column=1, row=0)
        p1 = Label(
            frame,
            text='Player One:',
            bg='white'
        )
        p1.grid(sticky='sw',column=0, row=1)
        p2 = Label(
            frame,
            text='Player Two:',
            bg='white'
        )
        p2.grid(sticky='sw',column=1, row=1)

        self.lbox1 = ttk.Combobox(
            frame,
            height=5,
            values=SelectionStrings
        )
        self.lbox1.current(1)
        self.lbox1.grid(sticky='nwe',column=0, row=2)
        self.lbox2 = ttk.Combobox(
            frame,
            height=5,
            values=SelectionStrings
        )
        self.lbox2['values'] = SelectionStrings

        self.lbox2.current(2)
        self.lbox2.grid(sticky='nwe',column=1, row=2)
        Search_Depth1 = Label(
            frame,
            text='Search Depth',
            bg='white'
        )
        Search_Depth1.grid(sticky='swe',column=0, row=3)
        Search_Depth2 = Label(
            frame,
            text='Search Depth',
            bg='white'
        )
        Search_Depth2.grid(sticky='swe', column=1, row=3)
        player1depthslider = Scale(frame, from_=1, to_=21, orient='horizontal', variable=self.depth_player_1, bg='white')
        player1depthslider.set(2)
        player1depthslider.grid(sticky='nwe',column=0, row=4)
        player2depthslider = Scale(frame, from_=1, to_=21, orient='horizontal', variable=self.depth_player_2, bg='white')
        player2depthslider.set(2)
        player2depthslider.grid(sticky='nwe',column=1, row=4)
        vb_check = Checkbutton(frame, text="Show Tree", variable=self.verbose, bg='white')
        vb_check.grid(sticky='w',column=0, row=5)
        vb_check_compact = Checkbutton(frame, text="Compact Form", variable=self.verbose_compact, bg='white')
        vb_check_compact.grid(sticky='w', column=0, row=6)
        rd_check = Checkbutton(frame, text="Random Start", variable=self.rand_first_start, bg='white')
        rd_check.grid(sticky='w',column=1, row=5)
        rd_player_check = Checkbutton(frame, text="Random Starting Player", variable=self.rand_first_player, bg='white')
        rd_player_check.grid(sticky='w', column=1, row=6)
        start = Button(
            frame,
            text='Start',
            font=("Arial", 14),
            command=self.on_start_click,
            bg='white'
        )
        start.grid(sticky='we',column=0, row=7, columnspan=2)

        Test_Repeats_Label = Label(
            frame,
            text='Test Repeats',
            bg='white'
        )
        Test_Repeats_Label.grid(sticky='sw',column=0, row=8)
        self.test_repeats = Entry(frame, bg='white')
        self.test_repeats.insert(END, '1')
        self.test_repeats.grid(sticky='nw',column=0, row=9)

        start_test = Button(
            frame,
            text='Start Full Test',
            font=("Arial", 14),
            command=self.on_start_full_test_click,
            bg='white'
        )
        start_test.grid(sticky='nwe',column=0, row=10, columnspan=2)
        train_neural_net = Button(
            frame,
            text='train_neural_net',
            font=("Arial", 8),
            command=neural_net.create_and_train,
            bg='white'
        )
        train_neural_net.grid(sticky='nwe', column=1, row=9)
        neural_net_training_data = Button(
            frame,
            text='Gather Training Data',
            font=("Arial", 8),
            command=self.neural_net_training_data_click,
            bg='white'
        )
        neural_net_training_data.grid(sticky='swe', column=1, row=8)
        credit = Label(
            frame,
            font=("Arial", 7),
            text='Written by Riley Mullett',
            bg='white'
        )
        credit.grid(sticky='sw', column=2, row=0)
        ep_warning = Label(
            frame,
            font=("Arial", 7),
            text='WARNING: A small percentage of people may experience seizures \nwhen exposed to the content of this program. \nPlease ensure the checkbox below is checked to mitigate risk. \nIt is not a guarantee of prevention.',
            bg='red'
        )
        ep_warning.grid(sticky='sw', column=2, columnspan=2, row=9)
        ep_check = Checkbutton(frame, text="Epilepsy safety buffer", variable=self.epilepsy_control, bg='white')
        ep_check.grid(sticky='w', column=3, row=10)
        ep_check.select()
        TutorialFrame = tkinter.Frame(frame, bg='white')
        Tutorial1 = Button(
            TutorialFrame,
            text='Tutorial 1: Overview',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial1),
            bg='white'
        )
        Tutorial1.grid(sticky='we', column=0, row=0)
        Tutorial2 = Button(
            TutorialFrame,
            text='Tutorial 2: Search',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial2),
            bg='white'
        )
        Tutorial2.grid(sticky='we', column=0, row=1)
        Tutorial3 = Button(
            TutorialFrame,
            text='Tutorial 3: Adversarial Agents',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial3),
            bg='white'
        )
        Tutorial3.grid(sticky='we', column=0, row=2)
        Tutorial4 = Button(
            TutorialFrame,
            text='Tutorial 4: Minimax',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial4),
            bg='white'
        )
        Tutorial4.grid(sticky='we', column=0, row=3)
        Tutorial5 = Button(
            TutorialFrame,
            text='Tutorial 5: Alpha-Beta Pruning',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial5),
            bg='white'
        )
        Tutorial5.grid(sticky='we', column=0, row=4)
        Tutorial6 = Button(
            TutorialFrame,
            text='Tutorial 6: Neural Networks',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial6),
            bg='white'
        )
        Tutorial6.grid(sticky='we', column=0, row=5)
        Tutorial7 = Button(
            TutorialFrame,
            text='Tutorial 7: Add your own agent',
            font=("Arial", 10),
            command=lambda: self.start_tut(tutorials.Tutorial7),
            bg='white'
        )
        Tutorial7.grid(sticky='we', column=0, row=6)
        TutorialFrame.grid(sticky='enw', column=3, row=2, columnspan=2, rowspan=10)
        frame.pack()

    def start_tut(self, tutorial):
        tut = tutorial_ui.tutorial_ui(root=self)
        tut.setup(tutorial)

    def on_start_click(self):
        flipped_start = False
        idxs1 = self.lbox1.current()
        idxs2 = self.lbox2.current()
        playerone = SelectionPlayers[idxs1]
        playertwo = SelectionPlayers[idxs2]
        depth1 = self.depth_player_1.get()
        depth2 = self.depth_player_2.get()
        if not (self.rand_first_player.get() == 0):
            if randint(0, 1) == 1:
                flipped_start = True
                playerone = SelectionPlayers[idxs2]
                playertwo = SelectionPlayers[idxs1]
                depth1 = self.depth_player_2.get()
                depth2 = self.depth_player_1.get()
        predetermined_start = -1
        if not self.rand_first_start.get() == 0:
            predetermined_start = randint(0, 6)
        any_human_players = False

        if playerone == human or playertwo == human:
            any_human_players = True

        is_game_verbose = True
        if self.verbose.get() == 0:
            is_game_verbose = False
        is_verbose_non_compact = False
        if self.verbose_compact.get() == 0:
            is_verbose_non_compact = True
        game = Game(players=[playerone, playertwo], verbose=is_game_verbose, non_compact=is_verbose_non_compact, predetermined_start=predetermined_start, player_search_depths=[depth1, depth2], stat_creation_run=False)
        x = threading.Thread(target=self.start_thread, args=(game, any_human_players, flipped_start, idxs1, idxs2))
        x.start()

    def start_thread(self, game: Game, any_human_players, flipped_start, idxs1: str, idxs2: str):
        view = game_UI.GameUI(game=game, root=self, any_human_players=any_human_players)
        view.setup()
        view.title = 'Game Window'
        self.update()
        if self.epilepsy_control.get() == 1:
            sleep(1)
        victor = game.begin(view=view, root=self)

        if flipped_start:
            stat_flipper = deepcopy(victor[LEFT_PLAYER])
            victor[LEFT_PLAYER] = victor[RIGHT_PLAYER]
            victor[RIGHT_PLAYER] = stat_flipper
        if victor[LEFT_PLAYER][STAT_VICTOR] == 1 and victor[RIGHT_PLAYER][STAT_VICTOR] == 0:
            print("Winner was " + SelectionStrings[idxs1] + ' (left player), closing window in 5 seconds...')
        elif victor[LEFT_PLAYER][STAT_VICTOR] == 0 and victor[RIGHT_PLAYER][STAT_VICTOR] == 1:
            print("Winner was " + SelectionStrings[idxs2] + ' (right player), closing window in 5 seconds...')
        else:
            print("Draw, closing window in 5 seconds...")
        sleep(5)
        if view is not None:  # Sometimes the user will close the window and this will throw a benign error
            view.destroy()

    def on_start_full_test_click(self):
        try:
            repeats = int(self.test_repeats.get())
            if repeats != 0:
                self.Test_Stats = [[[0 for _ in range(5)] for _ in range(2)] for _ in range(14 * repeats)]
                idxs1 = self.lbox1.current()
                idxs2 = self.lbox2.current()
                player_one = SelectionPlayers[idxs1]
                player_two = SelectionPlayers[idxs2]
                if player_one == human or player_two == human:
                    print("This test does not support human opponents.")
                else:
                    print("You have selected a full test. \nGames will be run with alternating first agents and first moves. \nStatistics will be printed at the end.")
                    for repeat_count in range(0, repeats):
                        for starting_point in range(0, 7):
                            self.Test_Stats[(14 * repeat_count) + starting_point * 2][LEFT_PLAYER][STAT_TURNS] += 1  # This is needed due to the forced start
                            game = Game(players=[player_one, player_two], verbose=False, non_compact=False, predetermined_start=starting_point, player_search_depths=[self.depth_player_1.get(), self.depth_player_2.get()], stat_creation_run=False)
                            self.run_test_with_parameters(False, (14 * repeat_count) + starting_point * 2, game)
                            self.Test_Stats[(14 * repeat_count) + starting_point * 2 + 1][RIGHT_PLAYER][STAT_TURNS] += 1  # This is needed due to the forced start
                            game = Game(players=[player_two, player_one], verbose=False, non_compact=False, predetermined_start=starting_point, player_search_depths=[self.depth_player_2.get(), self.depth_player_1.get()], stat_creation_run=False)
                            self.run_test_with_parameters(True, (14 * repeat_count) + starting_point * 2 + 1, game)
                    print("Tests Complete")
                    collate_data_for_player(LEFT_PLAYER, SelectionStrings[idxs1], self.Test_Stats, repeats)
                    collate_data_for_player(RIGHT_PLAYER, SelectionStrings[idxs2], self.Test_Stats, repeats)
            else:
                print("Please enter a valid number of test repeats.")

        except ValueError:
            print("Please enter a valid number of test repeats.")

    def run_test_with_parameters(self, flipped_start, stat_array_position, game: Game):
        view = game_UI.GameUI(game=game, root=self, any_human_players=False)
        view.setup()
        self.update()
        if self.epilepsy_control.get() == 1:
            sleep(1)
        self.Test_Stats[stat_array_position] = game.begin(view=view, root=self)
        if self.epilepsy_control.get() == 1:
            sleep(1.5)
        if flipped_start:
            stat_flipper = deepcopy(self.Test_Stats[stat_array_position][LEFT_PLAYER])
            self.Test_Stats[stat_array_position][LEFT_PLAYER] = deepcopy(self.Test_Stats[stat_array_position][RIGHT_PLAYER])
            self.Test_Stats[stat_array_position][RIGHT_PLAYER] = stat_flipper
        if view is not None:  # Sometimes the user will close the window and this will throw a benign error
            view.destroy()

    def neural_net_training_data_click(self):
        CSV_FILE = ""
        TRAINING_DEPTH = 7
        print("You are generating a new dataset for neural network training. \nThis may take some time.")
        for depth_measure in range(1, TRAINING_DEPTH):
            for starting_point in range(0, 7):
                game = Game(players=[minimax_alphabeta_unpredictable, minimax_alphabeta_unpredictable], verbose=False, non_compact=False, predetermined_start=starting_point, player_search_depths=[TRAINING_DEPTH, depth_measure], stat_creation_run=True)
                CSV_FILE = CSV_FILE + self.database_creation_sim(False, game)
                game = Game(players=[minimax_alphabeta_unpredictable, minimax_alphabeta_unpredictable], verbose=False, non_compact=False, predetermined_start=starting_point, player_search_depths=[depth_measure, TRAINING_DEPTH], stat_creation_run=True)
                CSV_FILE = CSV_FILE + self.database_creation_sim(True, game)
        print("dataset Complete. It will now be saved to disk as connect-4-train.csv.")
        CSV_ARRAY = CSV_FILE.split('!')
        with open('connect-4-train.csv', 'w') as file:
            for _ in CSV_ARRAY:
                file.write(_)
                file.write('\n')
        file.close()

    def database_creation_sim(self, flipped_start, game: Game) -> str:
        view = game_UI.GameUI(game=game, root=self, any_human_players=False)
        view.setup()
        self.update()
        if self.epilepsy_control.get() == 1:
            sleep(1)
        CSV_FILE = game.begin(view=view, root=self, db_creation_flipped_start=flipped_start)
        if self.epilepsy_control.get() == 1:
            sleep(1.5)
        if view is not None:  # Sometimes the user will close the window and this will throw a benign error
            view.destroy()
        return CSV_FILE


def main():
    window = ConnectFourAI()
    window.set_up()

    window.mainloop()


if __name__ == "__main__":
    main()
