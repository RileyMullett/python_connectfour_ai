import time
from branches_UI import Branches_UI
from time import sleep
from agents.genericPlayer import genericPlayer
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
print("Importing Tensorflow, this sometimes takes a little while...")
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPool2D
from tensorflow.keras.optimizers import Adam

my_devices = tf.config.experimental.list_physical_devices(device_type='CPU')
tf.config.experimental.set_visible_devices(devices=my_devices, device_type='CPU')
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4
NOTHING = 0
"""If you're unfamiliar with neural networks, I would strongly recommend going through some crash courses on youtube first.
The basic concept is to feed the agent a huge packet of situations and their correct answers, hoping that it discovers the pattern which is used to classify them
In our case, we treat the game board like a 7*6 pixel image, and the agent looks at it like a photograph, and based on it's previous knowledge of 7*6 pixel images,
tries to predict the correct next move. you may notice that the neural network can still be wrong, and can still lose often. There are several possible reasons for this, including
- Over-fitting the data, which makes the neural network less diverse
- Incorrect, imperfect or low quality data, which is the equivalent of pulling a prank on the neural network
"""


class neural_net(genericPlayer):
    @staticmethod
    def take_turn(game=None, player = None, verbose = None, non_compact = None, root=None, search_depth = None, game_stat_cur_player= None) -> int:
        model = None
        try:
            model = tf.keras.models.load_model('pretrained_neural_net')
        except ImportError and IOError:
            print('You have no pretrained neural network! make one!')
        current_turn = np.zeros((6, 7))
        for y in range(0, 6):
            for x in range(0, 7):
                current_turn[y][x] = game.board_pieces[x][y]
        current_turn = np.reshape(current_turn, (-1, 6, 7, 1))
        start = time.time()
        turn = model.predict(x=current_turn)
        end = time.time()
        game_stat_cur_player[STAT_TURNS] += 1
        game_stat_cur_player[STAT_TURN_TIME] += end - start
        comparison = 0
        column_choice = -1
        for COLUMN in range(0, 7):
            if turn[0][COLUMN] > comparison and game.board_pieces[COLUMN][5] == NOTHING:  # The Neural network may not understand the limitations of the board itself.
                comparison = turn[0][COLUMN]
                column_choice = COLUMN
        if verbose:
            decision_tree = [None, None, [None] * 7]
            decision_tree[0] = str(comparison)[:4]
            decision_tree[1] = column_choice
            for x in range(7):
                decision_tree[2][x] = [str(turn[0][x])[:4], 0, None]
            view = Branches_UI(game=game, root=root, compute_time=end - start, max_depth=1)
            view.setup(decision_tree=decision_tree, current_branch=decision_tree, players_turn=True, non_compact=True, branch_above=None, is_neural_net=True)
            view.update()
            while not view.continue_game:
                sleep(0.1)
        return column_choice

    @staticmethod
    def create_and_train():
        model = Sequential([
            Conv2D(filters=64, kernel_size=(4, 4), activation='relu', padding='same', input_shape=(6, 7, 1)),
            Conv2D(filters=64, kernel_size=(2, 2), activation='relu', padding='same'),
            MaxPool2D(pool_size=(2, 2), strides=2),
            Flatten(),
            Dense(units=7, activation='softmax')
        ])

        model.compile(optimizer=Adam(learning_rate=0.0001), loss='categorical_crossentropy', metrics=['accuracy'])
        training_data = np.genfromtxt("connect-4-train.csv", dtype='int', delimiter=",")

        x_train = training_data[1:, :42]
        x_train = np.reshape(x_train, (-1, 6, 7, 1))  # Data is currently just a row of 42 lines, needs to resemble a connect 4 board for optimal results
        # print(x_train.shape)
        y_train = training_data[1:, 42]
        b = np.zeros((y_train.size, y_train.max() + 1))
        b[np.arange(y_train.size), y_train] = 1
        y_train = b
        # print(y_train.shape)
        model.summary()
        model.fit(
            x_train,
            y_train,
            epochs=2000
        )
        model.save('pretrained_neural_net')
