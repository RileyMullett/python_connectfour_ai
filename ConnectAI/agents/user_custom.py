from copy import deepcopy
from random import seed
import time
from agents.genericPlayer import genericPlayer

STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4
NOTHING = 0


class user_custom(genericPlayer): # YOU NEED TO CHANGE THE NAME OF THIS CLASS FOR IT TO IMPORT, IT CAN BE NAMED ANYTHING YOU LIKE
    seed(time.time())

    def take_turn(game=None, player = None, verbose= None, non_compact= None, root=None, search_depth = None, game_stat_cur_player = None) -> int:
        """ If you have any interesting ideas for a custom competitive agent, you can write your logic here.
         simply return the column which the agent is placing their token in the form of an integer, from 0 to 6, left to right.
         the 'genericplayer' abstract class can accept a large amount of input variables (typecast for readability), but you don't *need* to use any.
         in most use-cases, you'll need to read the current_board_array to get an idea of the game's current state. This 2D array represents the board
         in terms of numbers. 0 is empty, 1 and 2 are the various players.
         if you make anything awesome, ask Hannes to let me know. - RM"""
        current_board_array = deepcopy(game.board_pieces)
        game_stat_cur_player[STAT_TURNS] += 1  # Useful if you want to log statistics
        return 0
