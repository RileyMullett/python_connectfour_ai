from agents.genericPlayer import genericPlayer
from copy import deepcopy
import time
from time import sleep
from branches_UI import Branches_UI
import threading
import numbers
NUM_NONE = -999999 # Suitably large number to replace none
NOTHING = 0
YELLOW = 1
RED = 2
first_depth = 1
BEST_SCORE = 0
BEST_COLUMN = 1
CHILD_TREES = 2
STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4

"""This class contains functions used by both naive minimax and minimax with alphabeta pruning."""


class minimax(genericPlayer):
    def __init__(self):  # Instantiating window
        super().__init__()

    @staticmethod
    def take_turn(game=None, player = None, verbose = None, non_compact = None, root=None, search_depth = None, game_stat_cur_player = None) -> int:
        """Top level of the minimax function, setting up parameters and handling win/lose conditions."""
        max_depth = search_depth
        theoretical_board_state = deepcopy(game.board_pieces)
        is_player_yellow = True
        if player == RED:
            is_player_yellow = False

        start = time.time()
        decision_tree=minimax.search(cur_depth=first_depth, maxi_depth=max_depth, theoretical_board_state=theoretical_board_state, is_player_yellow=is_player_yellow, cur_player=True)
        end = time.time()
        game_stat_cur_player[STAT_TURN_TIME] += end - start
        if verbose:
            view = Branches_UI(game=game, root=root, compute_time=end - start, max_depth=max_depth)
            view.setup(decision_tree=decision_tree, current_branch=decision_tree, players_turn=True, non_compact=non_compact, branch_above=NUM_NONE, is_neural_net=False)
            view.update()
            while not view.continue_game:
                sleep(0.1)
        game_stat_cur_player[STAT_TURNS] += 1
        if decision_tree[BEST_COLUMN] is NUM_NONE:  # A rare edge-case which has minimax always lose.
            for COLUMN in range(0, 7):
                for ROW in range(0, 6):
                    if theoretical_board_state[COLUMN][ROW] == NOTHING:
                        return COLUMN
        return decision_tree[BEST_COLUMN]

    @staticmethod
    def search(cur_depth, maxi_depth, theoretical_board_state, is_player_yellow, cur_player):
        """This is a recursive function, meaning it will call children versions of itself many many times.
        this is necessary to generate the exponential number of possible states that need to be evaluated"""
        # Create an array, which has arrays of seven within it
        # each array should be a value, and an array of 7 children.
        # decision_tree[Value, BestColumn, Children] where children is an array of 7 descision trees.

        player = RED
        if cur_player and is_player_yellow:
            player = YELLOW
        else:
            if not is_player_yellow:
                player = YELLOW
        decision_tree = [100000, NUM_NONE, [NUM_NONE]*7] # Initialise the new section of the tree and it's children, starting with each child in a worst case scenario
        if cur_player:
            decision_tree[BEST_SCORE] = -100000  # Initialise the new section of the tree and it's children, starting with each child in a worst case scenario

        for COLUMN in range(0, 7):
            for ROW in range(0, 6):
                if theoretical_board_state[COLUMN][ROW] == NOTHING:
                    new_theoretical_board_state = deepcopy(theoretical_board_state)
                    new_theoretical_board_state[COLUMN][ROW] = player
                    b_continue = True

                    if cur_player:
                        var = minimax.win_condition(new_theoretical_board_state, is_player_yellow, COLUMN, ROW)
                        if var is not False:  # This is a win  condition for player
                            decision_tree[CHILD_TREES][COLUMN] = [4 * var, COLUMN, NUM_NONE]
                            b_continue = False
                        else:
                            var = minimax.win_condition(new_theoretical_board_state, not is_player_yellow, COLUMN, ROW)
                            if var is not False: # This is a lose  condition for player
                                decision_tree[CHILD_TREES][COLUMN] = [4 * var, COLUMN, NUM_NONE]
                                b_continue = False
                    else:
                        var = minimax.win_condition(new_theoretical_board_state, not is_player_yellow, COLUMN, ROW)
                        if var is not False:  # This is a win  condition for other player
                            decision_tree[CHILD_TREES][COLUMN] = [-4 * var, COLUMN, NUM_NONE]
                            b_continue = False
                        else:
                            var = minimax.win_condition(new_theoretical_board_state, is_player_yellow, COLUMN, ROW)
                            if var is not False: # This is a lose  condition for other player
                                decision_tree[CHILD_TREES][COLUMN] = [-4 * var, COLUMN, NUM_NONE]
                                b_continue = False

                    if b_continue:
                        if cur_depth == maxi_depth:
                            column_value = minimax.final_depth_scoring(theoretical_board_state, is_player_yellow, COLUMN, ROW)
                            decision_tree[CHILD_TREES][COLUMN] = [column_value, COLUMN, NUM_NONE]
                        else:
                            decision_tree[CHILD_TREES][COLUMN] = minimax.search(cur_depth+1, maxi_depth, new_theoretical_board_state, is_player_yellow, not cur_player)

                    if cur_player:  # Maximizing
                        if not isinstance(decision_tree[CHILD_TREES][COLUMN], numbers.Integral):
                            if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] != NUM_NONE:
                                if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] > decision_tree[BEST_SCORE]:
                                    decision_tree[BEST_SCORE] = decision_tree[CHILD_TREES][COLUMN][BEST_SCORE]
                                    decision_tree[BEST_COLUMN] = COLUMN

                    else:  # Minimizing
                        if not isinstance(decision_tree[CHILD_TREES][COLUMN], numbers.Integral):
                            if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] != NUM_NONE:
                                if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] < decision_tree[BEST_SCORE]:
                                    decision_tree[BEST_SCORE] = decision_tree[CHILD_TREES][COLUMN][BEST_SCORE]
                                    decision_tree[BEST_COLUMN] = COLUMN
                    break

        return decision_tree

    @staticmethod
    def win_condition(potential, is_player_yellow, col, x) -> bool:
        """Checking for instant wins."""
        # Check win condition and tally the score
        new_score1 = minimax.score_search(is_player_yellow, col, x, 0, 1, potential, True)
        new_score2 = minimax.score_search(is_player_yellow, col, x, 1, 0, potential, True)
        new_score3 = minimax.score_search(is_player_yellow, col, x, 1, 1, potential, True)
        new_score4 = minimax.score_search(is_player_yellow, col, x, 1, -1, potential, True)
        if new_score1 >= 3 or new_score2 >= 3 or new_score3 >= 3 or new_score4 >= 3:
            return max(new_score1, new_score2, new_score3, new_score4)
        return False

    @staticmethod
    def final_depth_scoring(potential, is_player_yellow, col, x) -> int:
        """With minimax, you use a heuristic to determine the value of a state x turns in advance, and then
        work backwards to determine whether that state is viable to be sought after."""
        # Check win condition and tally the score
        new_score = 0
        new_score = minimax.score_search(is_player_yellow, col, x, 0, 1, potential, False)
        new_score += minimax.score_search(is_player_yellow, col, x, 1, 0, potential, False)
        new_score += minimax.score_search(is_player_yellow, col, x, 1, 1, potential, False)
        new_score += minimax.score_search(is_player_yellow, col, x, 1, -1, potential, False)
        return new_score

    @staticmethod
    def score_search(is_player_yellow, column, row, column_modifier, row_modifier, new_potential, instant_win):
        """The heuristic used here is a simple count of how many like tokens are currently in a row around the
        given position. There are other heuristics you can use, and I encourage you to create one yourself."""
        count = 0
        space = 0
        player = RED
        if is_player_yellow:
            player = YELLOW

        for x in range(1, 4):
            new_column_modifier = column_modifier * x
            new_row_modifier = row_modifier * x
            if (column + new_column_modifier < 0) or (row + new_row_modifier < 0) or (column + new_column_modifier > 6) or (row + new_row_modifier > 5):
                break
            else:
                _ = new_potential[column + new_column_modifier][row + new_row_modifier]
                if _ == player or (not instant_win and _ == 0):
                    if _ == player:
                        count += 1
                    space += 1
                else:
                    break
        for x in range(1, 4):
            new_column_modifier = column_modifier * x * (-1)
            new_row_modifier = row_modifier * x * (-1)
            if (column + new_column_modifier < 0) or (column + new_column_modifier > 6) or (row + new_row_modifier > 5) or (row + new_row_modifier < 0):
                break
            else:
                _ = new_potential[column + new_column_modifier][row + new_row_modifier]
                if _ == player or (not instant_win and _ == 0):
                    if _ == player:
                        count += 1
                    space += 1
                else:
                    break
        if space >= 3:
            return space + 4*count
        else:
            return 0
