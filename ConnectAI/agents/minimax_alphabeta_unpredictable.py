from agents.genericPlayer import genericPlayer
from agents.minimax_alphabeta import minimax_alphabeta
from copy import deepcopy
from agents.minimax import minimax
import time
from time import sleep
from branches_UI import Branches_UI
from random import randint, seed, random, choice
import random
NOTHING = 0
YELLOW = 1
RED = 2
first_depth = 1
BEST_SCORE = 0
BEST_COLUMN = 1
CHILD_TREES = 2
STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4
NUM_NONE = -999999
""" slower than minimax alphabeta, but more likely to win, over minimax alphabeta, because minimax can't predict it's draw-cases."""

class minimax_alphabeta_unpredictable(genericPlayer):

    pruned_branches = 0

    def __init__(self):  # Instantiating window
        super().__init__()
        seed(time.time())

    @staticmethod
    def take_turn(game=None, player = None, verbose = None, non_compact = None, root=None, search_depth = None, game_stat_cur_player = None) -> int:
        """Top level of the minimax function, setting up parameters and handling win/lose conditions."""
        minimax_alphabeta_unpredictable.pruned_branches = 0
        max_depth = search_depth
        theoretical_board_state = deepcopy(game.board_pieces)
        is_player_yellow = True
        if player == RED:
            is_player_yellow = False
        start = time.time()
        decision_tree = minimax_alphabeta.search(cur_depth=first_depth, maxi_depth=max_depth, theoretical_board_state=theoretical_board_state, is_player_yellow=is_player_yellow, cur_player=True, alpha=-99999999, beta=99999999)
        end = time.time()
        index=0
        randpool = []
        for result in range(7):
            if decision_tree[CHILD_TREES][result] != NUM_NONE:
                if decision_tree[CHILD_TREES][result][BEST_SCORE] == decision_tree[BEST_SCORE]:
                    randpool.append(index)
            index += 1
        val = random.randint(0, len(randpool)-1)
        decision_tree[BEST_COLUMN] = randpool[val]

        game_stat_cur_player[STAT_TURN_TIME] += end - start
        game_stat_cur_player[STAT_BRANCH_PRUNES] += minimax_alphabeta_unpredictable.pruned_branches

        if verbose:
            view = Branches_UI(game=game, root=root, compute_time=end - start, max_depth=max_depth)
            view.setup(decision_tree=decision_tree, current_branch=decision_tree, players_turn=True, non_compact=non_compact, branch_above=NUM_NONE, is_neural_net=False)
            view.update()
            while not view.continue_game:
                sleep(0.1)
        game_stat_cur_player[STAT_TURNS] += 1
        if decision_tree[BEST_COLUMN] is NUM_NONE:  # A rare edge-case which has minimax always lose.
            for COLUMN in range(0, 7):
                for ROW in range(0, 6):
                    if theoretical_board_state[COLUMN][ROW] == NOTHING:
                        return COLUMN
        return decision_tree[BEST_COLUMN]
