from random import seed
import time
from random import randint
from agents.genericPlayer import genericPlayer

STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4
NOTHING = 0

"""If you're new to coding, this might be the best example to help you get started with your own agent. 
It's the most basic agent I made. randint returns an integer within the bounds supplied. Seed is what 
generates the randomness. If you're unfamiliar with how randomness works on a computer, the web is your friend."""


class random_agent(genericPlayer):
    seed(time.time())

    @staticmethod
    def take_turn(game=None, player = None, verbose= None, non_compact = None, root=None, search_depth = None, game_stat_cur_player = None):
        game_stat_cur_player[STAT_TURNS] += 1
        x = randint(0, 6)
        while game.board_pieces[x][5] is not NOTHING:
            x = randint(0, 6)
        return x
