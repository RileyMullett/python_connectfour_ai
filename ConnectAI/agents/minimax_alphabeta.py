from agents.genericPlayer import genericPlayer
from copy import deepcopy
from agents.minimax import minimax
import time
from time import sleep
from branches_UI import Branches_UI
import numbers
NUM_NONE = -999999 # Suitably large number to replace none
NOTHING = 0
YELLOW = 1
RED = 2
first_depth = 1
BEST_SCORE = 0
BEST_COLUMN = 1
CHILD_TREES = 2
STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4


class minimax_alphabeta(genericPlayer):
    pruned_branches = 0

    def __init__(self):  # Instantiating window
        super().__init__()

    @staticmethod
    def take_turn(game=None, player = None, verbose = None, non_compact = None, root=None, search_depth = None, game_stat_cur_player = None) -> int:
        """Top level of the minimax function, setting up parameters and handling win/lose conditions."""
        minimax_alphabeta.pruned_branches = 0
        max_depth = search_depth
        theoretical_board_state = deepcopy(game.board_pieces)
        is_player_yellow = True
        if player == RED:
            is_player_yellow = False
        start = time.time()
        # import cProfile
        # import pstats
        # profile = cProfile.Profile()
        # profile.enable()

        decision_tree = minimax_alphabeta.search(cur_depth=first_depth, maxi_depth=max_depth, theoretical_board_state=theoretical_board_state, is_player_yellow=is_player_yellow, cur_player=True, alpha=-99999999, beta=99999999)
        # profile.disable()
        # ps = pstats.Stats(profile)
        # ps.print_stats()
        end = time.time()
        game_stat_cur_player[STAT_TURN_TIME] += end - start
        game_stat_cur_player[STAT_BRANCH_PRUNES] += minimax_alphabeta.pruned_branches
        if verbose:
            view = Branches_UI(game=game, root=root, compute_time=end - start, max_depth=max_depth)
            view.setup(decision_tree=decision_tree, current_branch=decision_tree, players_turn=True, non_compact=non_compact, branch_above=NUM_NONE, is_neural_net=False)
            view.update()
            while not view.continue_game:
                sleep(0.1)
        game_stat_cur_player[STAT_TURNS] += 1
        if decision_tree[BEST_COLUMN] is NUM_NONE:  # A rare edge-case which has minimax always lose.
            for COLUMN in range(0, 7):
                for ROW in range(0, 6):
                    if theoretical_board_state[COLUMN][ROW] == NOTHING:
                        return COLUMN
        return decision_tree[BEST_COLUMN]

    @staticmethod
    def search(cur_depth, maxi_depth, theoretical_board_state, is_player_yellow, cur_player, alpha, beta):
        """This is a recursive function, meaning it will call children versions of itself many many times.
            this is necessary to generate the exponential number of possible states that need to be evaluated"""
        # Create an array, which has arrays of seven within it
        # each array should be a value, and an array of 7 children.
        # decision_tree[Value, BestColumn, Children] where children is an array of 7 descision trees.

        player = RED
        if cur_player and is_player_yellow:
            player = YELLOW
        else:
            if not is_player_yellow:
                player = YELLOW

        decision_tree = [100000, NUM_NONE, [NUM_NONE]*7]  # Initialise the new section of the tree and it's children, starting with each child in a worst case scenario
        if cur_player:
            decision_tree[BEST_SCORE] = -100000  # Initialise the new section of the tree and it's children, starting with each child in a worst case scenario
        # ValidPositionChosen = False
        for COLUMN in range(0, 7):
            for ROW in range(0, 6):
                if theoretical_board_state[COLUMN][ROW] == NOTHING:
                    new_theoretical_board_state = deepcopy(theoretical_board_state)
                    new_theoretical_board_state[COLUMN][ROW] = player
                    b_continue = True

                    if cur_player:
                        var = minimax.win_condition(new_theoretical_board_state, is_player_yellow, COLUMN, ROW)
                        if var is not False:  # This is a win  condition for player
                            decision_tree[CHILD_TREES][COLUMN] = [4 * var, COLUMN, NUM_NONE]
                            b_continue = False
                        else:
                            var = minimax.win_condition(new_theoretical_board_state, not is_player_yellow, COLUMN, ROW)
                            if var is not False:
                                decision_tree[CHILD_TREES][COLUMN] = [4 * var, COLUMN, NUM_NONE]
                                b_continue = False
                    else:
                        var = minimax.win_condition(new_theoretical_board_state, not is_player_yellow, COLUMN, ROW)
                        if var is not False:  # This is a win  condition for player
                            decision_tree[CHILD_TREES][COLUMN] = [-4 * var, COLUMN, NUM_NONE]
                            b_continue = False
                        else:
                            var = minimax.win_condition(new_theoretical_board_state, is_player_yellow, COLUMN, ROW)
                            if var is not False:
                                decision_tree[CHILD_TREES][COLUMN] = [-4 * var, COLUMN, NUM_NONE]
                                b_continue = False
                    if b_continue:
                        if cur_depth == maxi_depth:
                            column_value = minimax.final_depth_scoring(theoretical_board_state, is_player_yellow, COLUMN, ROW)
                            decision_tree[CHILD_TREES][COLUMN] = [column_value, COLUMN, NUM_NONE]
                        else:
                            decision_tree[CHILD_TREES][COLUMN] = minimax_alphabeta.search(cur_depth+1, maxi_depth, new_theoretical_board_state, is_player_yellow, not cur_player, alpha=alpha, beta=beta)

                    """ While examining the children of a maximizer, if value of maximizer > beta, prune the rest of the children.
                    While examining the children of a minimizer, if value of minimizer < alpha, prune the rest of the children.
                    Alpha = best already explored option along the path to the root for the maximizer
                    Beta = best already explored option along the path to the root for the minimizer"""
                    if cur_player:  # Maximizing
                        if not isinstance(decision_tree[CHILD_TREES][COLUMN], numbers.Integral):
                            if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] != NUM_NONE:
                                if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] > decision_tree[BEST_SCORE]:
                                    decision_tree[BEST_SCORE] = decision_tree[CHILD_TREES][COLUMN][BEST_SCORE]
                                    decision_tree[BEST_COLUMN] = COLUMN
                                    # ValidPositionChosen = True
                                    alpha = max(alpha, decision_tree[BEST_SCORE])
                                    if beta <= alpha:
                                        minimax_alphabeta.pruned_branches += 1
                                        return decision_tree
                    else:  # Minimizing
                        if not isinstance(decision_tree[CHILD_TREES][COLUMN], numbers.Integral):
                            if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] != NUM_NONE:
                                if decision_tree[CHILD_TREES][COLUMN][BEST_SCORE] < decision_tree[BEST_SCORE]:
                                    decision_tree[BEST_SCORE] = decision_tree[CHILD_TREES][COLUMN][BEST_SCORE]
                                    decision_tree[BEST_COLUMN] = COLUMN
                                    # ValidPositionChosen = True
                                    beta = min(beta, decision_tree[BEST_SCORE])
                                    if beta <= alpha:
                                        minimax_alphabeta.pruned_branches += 1
                                        return decision_tree
                    break

        return decision_tree
