import abc


class genericPlayer(abc.ABC):
    @staticmethod
    @abc.abstractmethod
    def take_turn() -> int:
        """ Function body is only implemented in child classes """
        return 0
