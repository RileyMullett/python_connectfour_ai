from agents.genericPlayer import genericPlayer
import time


STAT_VICTOR = 0
STAT_TURN_TIME = 1
STAT_TURNS = 2
STAT_BRANCH_PRUNES = 3
STAT_FORCED_DEFENCES = 4


class human(genericPlayer):
    """If you're looking for example code to help you make your own agent, pick a different agent to reference!
    making this agent involved waiting for user input, which is potentially nasty when not handled correctly."""
    waiting = True
    choice = 0

    @staticmethod
    def take_turn(game=None, player = None, verbose = None, non_compact = None, root=None, search_depth = None, game_stat_cur_player = None) -> int:
        """Waits for the user to choose a column and returns it's value."""
        start = time.time()
        while human.waiting:
            time.sleep(0.2)
        end = time.time()
        game_stat_cur_player[STAT_TURN_TIME] += start - end
        human.waiting = True
        game_stat_cur_player[STAT_TURNS] += 1
        return human.choice
