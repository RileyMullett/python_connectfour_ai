import tkinter

from game import Game
from main import ConnectFourAI

SPRITE_SCALE = 2
CANT_PLACE = 9999
NOTHING = 0
YELLOW = 1
RED = 2
HUMAN = 0
colours = ['blue', 'yellow', 'red', 'white']
BOARD_CELL_SIZE = 60
BOARD_CHIP_SIZE_HIGH = BOARD_CELL_SIZE - 6
BOARD_CHIP_SIZE_LOW = 6

class GameUI(tkinter.Toplevel):

    def __init__(self, root: ConnectFourAI, game: Game, any_human_players: bool):
        super().__init__(root)
        self.title = 'Active Game Display'
        self.Game = game
        self.column_count = 7
        self.row_count = 7
        self.any_human_players = any_human_players
        self.board = tkinter.Frame(self, background=colours[3], width=(self.column_count+2)*BOARD_CELL_SIZE, height=self.row_count*BOARD_CELL_SIZE)
        self.grid_space = [[tkinter.Canvas for x in range(7)] for y in range(9)]

    def create_token(self, column, turn, row):
        self.grid_space[column + 1][row].create_oval(BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_HIGH, BOARD_CHIP_SIZE_HIGH, fill=colours[turn])

    def setup(self):
        self.board.pack()
        board_base = tkinter.Canvas(self.board, bg='white', width=(self.column_count+2)*BOARD_CELL_SIZE, height=BOARD_CELL_SIZE*2)
        board_base.config(highlightthickness=0)
        board_base.create_polygon(0, (BOARD_CELL_SIZE/2), BOARD_CELL_SIZE*2,  (BOARD_CELL_SIZE/2)*3, (self.column_count+2)*BOARD_CELL_SIZE, (BOARD_CELL_SIZE/2)*3, self.column_count*BOARD_CELL_SIZE, (BOARD_CELL_SIZE/2), fill=colours[0])
        board_base.grid(column=0, row=self.row_count, columnspan=9, rowspan=2, sticky="nw")
        for x in range(self.column_count):
            if self.any_human_players:
                this_button = ButtonParams(game=self.Game, column=x)
                new_button = tkinter.Button(
                    master=self.board,
                    text='▼',
                    width=7,
                    bg=colours[0],
                    command=this_button.on_click
                )
                new_button.grid(column=x+1, row=0)
            else:
                token = tkinter.Canvas(self.board, background=colours[3], width=BOARD_CELL_SIZE, height=0, highlightthickness=0, relief='ridge')
                token.grid(column=x, row=0)
        for x in range(self.column_count+2):
            self.board.columnconfigure(x, minsize=BOARD_CELL_SIZE, weight=0)
        for x in range(self.column_count):
            for y in range(self.row_count-1):
                self.grid_space[x + 1][y] = tkinter.Canvas(self.board, background=colours[0], width=BOARD_CELL_SIZE, height=BOARD_CELL_SIZE, highlightthickness=0, relief='ridge')
                self.grid_space[x + 1][y].create_oval(BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_LOW, BOARD_CHIP_SIZE_HIGH, BOARD_CHIP_SIZE_HIGH, fill=colours[3])
                self.grid_space[x + 1][y].grid(row=(self.row_count - y), column=x+1, sticky="nw")
        players = tkinter.Label(self.board, text='Yellow is ' + str(self.Game.players[0])+'\nRed is ' + str(self.Game.players[1]), anchor='w')
        players.grid(column=0, columnspan=self.column_count + 4, row=self.row_count+1, sticky='w')


class ButtonParams:
    def __init__(self, game: Game, column):
        super().__init__()
        self.Game = game
        self.column = column

    def on_click(self):
        try:
            self.Game.players[self.Game.turn - 1].choice = self.column
            self.Game.players[self.Game.turn - 1].waiting = False
        except IOError:
            pass  # Expected to occur
